Source: lbcd
Section: net
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper (>= 11),
 init-system-helpers (>= 1.50),
 libsystemd-dev [linux-any],
 libtest-minimumversion-perl,
 libtest-pod-perl,
 libtest-strict-perl,
 pkgconf,
 systemd-dev
Rules-Requires-Root: no
Standards-Version: 4.1.3
Homepage: https://www.eyrie.org/~eagle/software/lbcd/
Vcs-Git: https://salsa.debian.org/debian/lbcd.git -b debian/master
Vcs-Browser: https://salsa.debian.org/debian/lbcd

Package: lbcd
Architecture: any
Multi-Arch: foreign
Depends:
 adduser,
 libio-socket-inet6-perl,
 ${misc:Depends},
 ${perl:Depends},
 ${shlibs:Depends},
Description: Return system load via UDP for remote load balancers
 lbcd is a daemon that answers UDP queries for system load information and
 returns such information as uptime, load, number of logged-in users,
 percentage free of /tmp and /var/tmp, and whether there is a user on the
 console.  It is intended for use with a load balancing system, and is
 particularly useful for such things as UNIX clusters for remote login
 where a traditional hardware load balancing solution doesn't work as well.
 .
 No load balancing system is included in this package, only the client
 daemon and a simple Perl script to query it.  No security or access
 control is done by the daemon, so access control must be done via
 iptables, a firewall, or an equivalent system.
